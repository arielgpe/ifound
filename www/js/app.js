// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('ifoundApp', ['ionic', 'ngResource', 'ngSanitize'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // setup an abstract state for the tabs directive
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })

    // Each tab has its own nav history stack:

    .state('tab.latest', {
      url: '/latest',
      views: {
        'tab-latest': {
          templateUrl: 'templates/tab-latest.html',
          controller: 'LatestCtrl'
        }
      }
    })
    .state('tab.latest-detail', {
      url: '/latest/:id',
      views: {
        'tab-latest': {
          templateUrl: 'templates/latest-detail.html',
          controller: 'LatestDetailCtrl'
        }
      }
    })

    .state('tab.popular', {
      url: '/popular',
      views: {
        'tab-popular': {
          templateUrl: 'templates/tab-popular.html',
          controller: 'PopularCtrl'
        }
      }
    })

    .state('tab.popular-detail', {
     url: '/popular/:id',
     views: {
       'tab-popular': {
         templateUrl: 'templates/popular-detail.html',
         controller: 'PopularDetailCtrl'
       }
     }
    })
    .state('tab.friend-detail', {
      url: '/friend/:friendId',
      views: {
        'tab-friends': {
          templateUrl: 'templates/friend-detail.html',
          controller: 'FriendDetailCtrl'
        }
      }
    })

    .state('tab.random', {
      url: '/random',
      views: {
        'tab-random': {
          templateUrl: 'templates/tab-random.html',
          controller: 'RandomCtrl'
        }
      }
    })

	 .state('tab.browse', {
      url: '/browse',
      views: {
        'tab-browse': {
          templateUrl: 'templates/tab-browse.html',
          controller: 'BrowseCtrl'
        }
      }
    })

    .state('tab.browse-selected', {
     url: '/browse/selected/:id',
     views: {
       'tab-browse': {
         templateUrl: 'templates/browse-selected.html',
         controller: 'SelectedCtrl'
       }
     }
   })
   .state('tab.browse-selected-item', {
    url: '/browse/item/:id',
    views: {
      'tab-browse': {
        templateUrl: 'templates/browse-selected-item.html',
        controller: 'SelectedItemCtrl'
      }
    }
  })

	 .state('tab.more', {
      url: '/more',
      views: {
        'tab-more': {
          templateUrl: 'templates/tab-more.html',
          controller: 'MoreCtrl'
        }
      }
    })

    .state('tab.more-search', {
     url: '/more/search',
     views: {
       'tab-more': {
         templateUrl: 'templates/search.html',
         controller: 'SearchCtrl'
       }
     }
 })

    .state('tab.more-search-detail', {
     url: '/more/search/:id',
     views: {
       'tab-more': {
         templateUrl: 'templates/search-detail.html',
         controller: 'SearchDetailCtrl'
       }
     }
 });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/latest');

});
