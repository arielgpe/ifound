var url = 'http://www.popcorndesign.co.uk/clients/ifound/api/get_recent_posts/?callback=JSON_CALLBACK';
angular.module('ifoundApp')

.controller('PopularCtrl', function($scope, Friends, $http, $ionicLoading, ResourceService, RestfullService) {
    // $scope.friends = RestfullService.get();
    $scope.status = true;
    var ooo = [];
    $ionicLoading.show({
        template: 'Loading...'
    });
    ResourceService.jsonp(function(data) {
        $scope.status = true;
        data.posts.forEach(function(entry) {
            var datos = {
                title: entry.title,
                tags: entry.tags,
                id: entry.id,
                thumbnail: entry.thumbnail,
                views: Number(entry.custom_fields.views)
            };
            ooo.push(datos);
        });
        $scope.friends = ooo;
        console.log(ooo);
    }, function(e) {
        $scope.status = false;
    });
    // .$promise.finally(function() {
    //     $ionicLoading.hide()
    // })

    $scope.doRefresh = function() {
        var ooo = [];
        ResourceService.jsonp(function(data) {
            $scope.status = true;
            data.posts.forEach(function(entry) {
                var datos = {
                    title: entry.title,
                    tags: entry.tags,
                    id: entry.id,
                    thumbnail: entry.thumbnail,
                    views: Number(entry.custom_fields.views)
                };
                ooo.push(datos);
            });
            $scope.friends = ooo;
            console.log(ooo);
        }, function(e) {
            $scope.status = false;
        });
        $scope.$broadcast('scroll.refreshComplete');
    };
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
    $scope.friend = Friends.get($stateParams.friendId);
})

.controller('RandomCtrl', function($scope, $state, $http, $window, $ionicLoading, ResourceService, RestfullService, $ionicTabsDelegate) {
    $ionicLoading.show({
        template: 'Loading...'
    });
    $scope.status = true;
    $scope.data = '';
    $scope.doRefresh = function() {
        ResourceService.jsonp(function(data) {
            $scope.status = true;
            var random = Math.floor((Math.random() * data.posts.length));
            $scope.data = data.posts[random];
            $scope.url = function() {
                $window.open(data.posts[random].url,'_blank','location=yes');
            };
        }, function(e) {
            $scope.status = false;
        });
        // console.log(happening)
        $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.checkTab = function(){
        var active = $ionicTabsDelegate.selectedIndex();
        if (active === 2){

        $state.go($state.$current, null, { reload: true });
        }
        else{
            $ionicTabsDelegate.select(2, true);
        }
    };

    ResourceService.jsonp(function(data) {

        $scope.status = true;
        var random = Math.floor((Math.random() * data.posts.length));
        $scope.data = data.posts[random];
        $scope.url = function() {
            $window.open(data.posts[random].url,'_blank','location=yes');
        };
    }, function(e) {
        $scope.status = false;
    });
    // .$promise.finally(function() {
    //     $ionicLoading.hide()
    // })
})

.controller('BrowseCtrl', function($scope, Friends, $http, $ionicLoading, ResourceService, RestfullService) {
    ResourceService.jsonp(function(data) {
        var arra = {};
        data.posts.forEach(function(entry) {
            entry.categories.forEach(function(categories) {
                arra[categories.id] = categories.title;
            });
        });
        $scope.data = arra;
    });
})

.controller('MoreCtrl', function($scope, Friends, $window) {
    $scope.url = function() {
        $window.open('http://urbanaesthetics.co.uk','_blank','location=yes');
    }
})

.controller('SearchCtrl', function($scope, $http, $ionicLoading, ResourceService, RestfullService) {
    var ooo = [];
    ResourceService.jsonp(function(data) {
        data.posts.forEach(function(entry) {

            var datos = {
                title: entry.title,
                tags: entry.tags,
                id: entry.id
            };
            ooo.push(datos);
        });

        console.log(ooo);
        $scope.data = ooo;
    });
})

.controller('SearchDetailCtrl', function($scope, $http, $stateParams, $window, $ionicLoading, ResourceService, RestfullService) {
    ResourceService.jsonp(function(data) {
        var object = null;
        data.posts.forEach(function(entry) {
            if (entry.id == $stateParams.id) {
                $scope.detail = entry;
                $scope.url = function() {
                    $window.open(entry.url,'_blank','location=yes');
                };
            }
        });
    });
})

.controller('PopularDetailCtrl', function($scope, $http, $stateParams, $window, $ionicLoading, ResourceService, RestfullService) {
    $scope.status = true;
    ResourceService.jsonp(function(data) {
        $scope.status = true;
        var object = null;
        data.posts.forEach(function(entry) {
            if (entry.id == $stateParams.id) {
                $scope.detail = entry;
                $scope.url = function() {
                    $window.open(entry.url,'_blank','location=yes');
                };
            }
        }, function(e) {
            $scope.status = false;
        });
    });
})

.controller('SelectedCtrl', function($scope, $http, $stateParams, $ionicLoading, ResourceService, RestfullService) {
    ResourceService.jsonp(function(data) {
        var arra = {};
        data.posts.forEach(function(entry) {
            entry.categories.forEach(function(categories) {
                if (categories.id == $stateParams.id) {
                    arra[entry.id] = [entry.title, entry.thumbnail];
                }
            });
        });
        $scope.data = arra;
        console.log(arra);
    });
})

.controller('SelectedItemCtrl', function($scope, $http, $stateParams, $window, $ionicLoading, ResourceService, RestfullService) {
    ResourceService.jsonp(function(data) {
        var object = null;
        data.posts.forEach(function(entry) {
            if (entry.id == $stateParams.id) {
                $scope.detail = entry;
                $scope.url = function() {
                    $window.open(entry.url,'_blank','location=yes');
                };
            }
        });
    });
});
