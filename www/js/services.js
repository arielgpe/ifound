angular.module('ifoundApp')

/**
 * A simple example service that returns some data.
 */
.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [
    { id: 0, name: 'Test Product 1' },
    { id: 1, name: 'Test Product 2' },
    { id: 2, name: 'Test Product 3' },
    { id: 3, name: 'Test Product 4' },
	{ id: 4, name: 'Test Product 5' },
    { id: 5, name: 'Test Product 6' }
  ];

  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    }
  };
});
