angular.module('ifoundApp')

.factory('ResourceService', function($http, $ionicLoading) {
    var promise = $http({method: 'JSONP', url: url});
    return {
        jsonp: function(data) {
            promise.success(data).finally(function() {
                $ionicLoading.hide();
            });
        }
    };
})

.factory('RestfullService', function($resource) {
    return $resource('http://www.popcorndesign.co.uk/clients/ifound/api/get_recent_posts/?dev=1');
});
