angular.module('ifoundApp')

.controller('LatestCtrl', function($scope, ResourceService, $http, $ionicLoading, RestfullService, $ionicPopup) {
    $scope.status = true;
    $ionicLoading.show({
        template: 'Loading...'
    });
    ResourceService.jsonp(function(data) {
        $scope.friends = data;
        $scope.status = true;
    }, function(e) {
        $scope.status = false;
    });
    // .$promise.finally(function() {
    //     $ionicLoading.hide()
    // });
    $scope.doRefresh = function() {
        ResourceService.jsonp(function(data) {
            $scope.friends = data;
            $scope.status = true;
        },function(e) {
            $scope.status = false;
        });
        $scope.$broadcast('scroll.refreshComplete');
    };
})

.controller('LatestDetailCtrl', function($scope, $http, $stateParams, $window, $ionicLoading, RestfullService,  ResourceService) {
    $scope.status = true;
    ResourceService.jsonp(function(data) {
        $scope.status = true;
        $scope.detail = data.posts[$stateParams.id];
        console.log(data.posts[$stateParams.id]);
        $scope.url = function() {
            $window.open(data.posts[$stateParams.id].url,'_blank','location=yes');
        };
    }, function(e) {
        $scope.status = false;
    });
});
